---
- name: Add IP address of all worker hosts to /etc/hosts
  lineinfile:
    dest: /etc/hosts
    regexp: '.*{{item}}$'
    line: "{{ hostvars[item].ansible_host }} {{ hostvars[item].inventory_hostname }}"
    state: present
  when: hostvars[item].ansible_host is defined
  with_items: "{{groups['bigdata_workers']}}"

- set_fact:
    san_hostnames: "{{san_hostnames|default()}}DNS:{{hostvars[item].inventory_hostname}},"
  with_inventory_hostnames:
    - bigdata_workers
  tags:
    - presto

- name: Generate private key and certificate for JKS
  shell: |
    openssl req \
      -newkey \
      rsa:4096 \
      -nodes \
      -keyout {{hostvars[item].inventory_hostname}}.presto-jks-private.key \
      -x509 \
      -days 7300 \
      -out {{hostvars[item].inventory_hostname}}.presto-jks-certificate.crt \
      -passin pass:{{java_key_store_password}} \
      -subj "/OU=presto/CN={{presto_domain_name}}/"
  args:
    chdir: "{{presto_keystore_path}}"
    creates: "{{presto_keystore_path}}/{{hostvars[item].inventory_hostname}}.presto-jks-certificate.crt"
  with_inventory_hostnames:
    - presto_coordinator
  tags:
    - presto

- name: Generate workers private keys and certificates for JKS
  shell: |
    openssl req \
      -newkey \
      rsa:4096 \
      -nodes \
      -keyout {{hostvars[item].inventory_hostname}}.presto-jks-private.key \
      -x509 \
      -days 7300 \
      -out {{hostvars[item].inventory_hostname}}.presto-jks-certificate.crt \
      -passin pass:{{java_key_store_password}} \
      -subj "/OU=presto/CN={{hostvars[item].inventory_hostname}}/"
  args:
    chdir: "{{presto_keystore_path}}"
    creates: "{{presto_keystore_path}}/{{hostvars[item].inventory_hostname}}.presto-jks-certificate.crt"
  with_inventory_hostnames:
    - bigdata_workers
  tags:
    - presto

- name: Generate workers and coordinator keystore files from private keys and certificates
  shell: |
    openssl pkcs12 \
      -inkey {{hostvars[item].inventory_hostname}}.presto-jks-private.key \
      -in {{hostvars[item].inventory_hostname}}.presto-jks-certificate.crt \
      -export \
      -out {{hostvars[item].inventory_hostname}}.presto-keystore.p12 \
      -passin pass:{{java_key_store_password}} \
      -passout pass:{{java_key_store_password}} \
      -name "{{hostvars[item].inventory_hostname}}"
  args:
    chdir: "{{presto_keystore_path}}"
    creates: "{{presto_keystore_path}}/{{hostvars[item].inventory_hostname}}.presto-keystore.p12"
  with_inventory_hostnames:
    - presto_coordinator:bigdata_workers
  tags:
    - presto

- name: Import coordinator and worker certificates in keystore file
  shell: |
    keytool -noprompt \
      -importkeystore \
      -srckeystore {{hostvars[item].inventory_hostname}}.presto-keystore.p12 \
      -srcstoretype pkcs12 \
      -srcstorepass {{java_key_store_password}} \
      -alias {{hostvars[item].inventory_hostname}} \
      -destkeystore presto_keystore.jks \
      -deststoretype JKS \
      -deststorepass {{java_key_store_password}} && \
    touch {{presto_keystore_path}}/{{hostvars[item].inventory_hostname}}.jks.imported
  args:
    chdir: "{{presto_keystore_path}}"
    creates: "{{presto_keystore_path}}/{{hostvars[item].inventory_hostname}}.jks.imported"
  with_inventory_hostnames:
    - presto_coordinator:bigdata_workers
  tags:
    - presto

- name: Import coordinator and worker certificates in truststore file
  shell: |
    keytool -noprompt \
      -import \
      -keystore presto_truststore.jks \
      -storepass {{java_key_store_password}} \
      -trustcacerts \
      -alias {{hostvars[item].inventory_hostname}} \
      -file {{hostvars[item].inventory_hostname}}.presto-jks-certificate.crt && \
    touch {{presto_keystore_path}}/{{hostvars[item].inventory_hostname}}_truststore.jks.imported
  args:
    chdir: "{{presto_keystore_path}}"
    creates: "{{presto_keystore_path}}/{{hostvars[item].inventory_hostname}}_truststore.jks.imported"
  with_inventory_hostnames:
    - presto_coordinator:bigdata_workers
  tags:
    - presto

- name: Import LDAP certificate in truststore file
  shell: |
    keytool -noprompt \
      -import \
      -keystore presto_truststore.jks \
      -storepass {{java_key_store_password}} \
      -trustcacerts \
      -alias {{openldap_server_domain_name}} \
      -file {{openldap_server_certificates_path}}/ldap.crt && \
    touch {{presto_keystore_path}}/ldap_truststore.jks.imported
  args:
    chdir: "{{presto_keystore_path}}"
    creates: "{{presto_keystore_path}}/ldap_truststore.jks.imported"
  tags:
    - presto

- name: Create .secrets file with keystore password
  template:
    src: secrets
    dest: "{{presto_user_home}}/.secrets"
    owner: "{{presto_user}}"
    group: "{{presto_user}}"
    mode: 0644
  tags:
    - presto

- name: Create presto-cli.sh wrapper script
  template:
    src: presto-cli.sh
    dest: "{{presto_binaries_path}}"
    owner: "{{presto_user}}"
    group: "{{presto_user}}"
    mode: 0764
  tags:
    - presto
