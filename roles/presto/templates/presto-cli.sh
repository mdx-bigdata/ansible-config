#!/bin/bash

source {{presto_user_home}}/.secrets
presto --server https://{{presto_domain_name}}:{{presto_server_https_port}} --keystore-path {{presto_keystore_path}}/presto_keystore.jks --keystore-password $PRESTO_KEYSTORE_PASS $@
