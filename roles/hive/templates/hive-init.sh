#!/bin/bash
# Hive startup script
#

PATH=/bin:/usr/bin:/sbin:/usr/sbin
SCRIPTNAME=$0
SLEEP_TIME=5
HIVE_USER={{hive_user}}
HIVE_LOG_DIR={{hive_log_dir}}
HIVE_LOG_FILE={{hive_log_dir}}/hive_{{item}}.log
HIVE_PID_FILE={{hive_log_dir}}/hive_{{item}}.pid
SETSID=/usr/bin/setsid
HIVE={{hive_binaries_path}}/hive

# Include default settings if available

# Ensure the setsid program exists
[ -x "$SETSID" ] || (echo "setsid package not installed" && exit 0)

#
# Setup Hive preconditions - default application settings
#
function hive_preconditions() {
    [ -r /etc/profile.d/java.sh ] && . /etc/profile.d/java.sh
    [ -r /etc/profile.d/hadoop.sh ] && . /etc/profile.d/hadoop.sh
    [ -r /etc/profile.d/hive.sh ] && . /etc/profile.d/hive.sh
}

#
# Handles starting the Hive service and ensuring that it started correctly and
# without problems.
#
function start() {
    echo "Hive {{item}} is starting" | tee -a ${HIVE_LOG_FILE}
    if [ -f ${HIVE_PID_FILE} ]; then
        echo "[`date "+%Y-%m-%d %H:%M:%S"`] [INFO] Checking PID..." >> ${HIVE_LOG_FILE}
        PID=$(cat ${HIVE_PID_FILE})
        ps -p $PID > /dev/null 2>&1
        if [ $? -eq 0 ]; then
            echo "[`date "+%Y-%m-%d %H:%M:%S"`] [WARN] Hive {{item}} is already running" >> ${HIVE_LOG_FILE}
            echo "Hive {{item}} is already running" | tee -a ${HIVE_LOG_FILE}
            exit 0
        fi
    fi
    echo "[`date "+%Y-%m-%d %H:%M:%S"`] [INFO] Starting Hive {{item}} and recording PID in the PID file..." >> ${HIVE_LOG_FILE}
    hive_preconditions
    $SETSID $HIVE --service {{item}} --hiveconf hive.log.dir=${HIVE_LOG_DIR} --hiveconf hive.log.file={{item}}.log > /dev/null 2>&1 < /dev/null &
    echo $! > ${HIVE_PID_FILE}
    if [ $? -ne 0 ]; then
        echo "[`date "+%Y-%m-%d %H:%M:%S"`] [ERROR] Could not create the PID file" >> ${HIVE_LOG_FILE}
        echo "Starting failed!" | tee -a ${HIVE_LOG_FILE}
        exit 1
    fi
    sleep $SLEEP_TIME
    echo "Hive {{item}} is started" | tee -a ${HIVE_LOG_FILE}
    exit 0
}

#
# Handles terminating the Hive service and ensuring that it terminated
# correctly.
#
function stop() {
    if [ -f ${HIVE_PID_FILE} ]; then
        echo "[`date "+%Y-%m-%d %H:%M:%S"`] [INFO] Checking PID..." >> ${HIVE_LOG_FILE}
        PID=$(cat ${HIVE_PID_FILE})
        ps -p $PID > /dev/null 2>&1
        if [ $? -eq 0 ]; then
            echo "Hive {{item}} is stopping" | tee -a ${HIVE_LOG_FILE}
            echo "[`date "+%Y-%m-%d %H:%M:%S"`] [INFO] Stopping Hive {{item}}..." >> ${HIVE_LOG_FILE}
            kill -9 $PID
            echo "[`date "+%Y-%m-%d %H:%M:%S"`] [INFO] Removing the PID file..." >> ${HIVE_LOG_FILE}
            rm -f ${HIVE_PID_FILE}
        fi
    fi
    echo "Hive {{item}} is stopped" | tee -a ${HIVE_LOG_FILE}
    exit 0
}

#
# Determines whether or not the service is running
#
function status() {
    if [ -f ${HIVE_PID_FILE} ]; then
        echo "[`date "+%Y-%m-%d %H:%M:%S"`] [INFO] Checking PID..." >> ${HIVE_LOG_FILE}
        PID=$(cat ${HIVE_PID_FILE})
        ps -p $PID > /dev/null 2>&1
        if [ $? -eq 0 ]; then
            echo "[`date "+%Y-%m-%d %H:%M:%S"`] [INFO] Hive {{item}} is running" >> ${HIVE_LOG_FILE}
            echo "Hive {{item}} is running" | tee -a ${HIVE_LOG_FILE}
            exit 0
        fi
    fi
    echo "[`date "+%Y-%m-%d %H:%M:%S"`] [INFO] Hive {{item}} is stopped" >> ${HIVE_LOG_FILE}
    echo "Hive {{item}} is stopped" | tee -a ${HIVE_LOG_FILE}
    exit 0
}

#
# Deal with the request
#
case "$1" in
  start)
    start
    ;;
  stop)
    stop
    ;;
  status)
    status
    ;;
  *)
    echo "Usage: $0 {start|stop|status}"
    exit 1
    ;;
esac

exit 0
