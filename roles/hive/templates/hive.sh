export HIVE_HOME={{hive_home}}
export HIVE_CONF_DIR={{hive_config_path}}
export PATH={{hive_binaries_path}}:$PATH
export CLASSPATH=$CLASSPATH:{{hadoop_libraries_path}}/*:.
export CLASSPATH=$CLASSPATH:{{hive_libraries_path}}/lib/*:.
