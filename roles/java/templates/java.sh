jdk_full_name=$(ls -l {{jvm_directory}} | grep "^d" | grep "java" | grep "{{java_main_version}}" | awk '{print$NF}')
export JAVA_HOME={{jvm_directory}}/$jdk_full_name
