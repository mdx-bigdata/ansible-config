#!/bin/bash

source ~/.bashrc
source /etc/profile.d/*.sh
nohup start-dfs.sh
nohup start-yarn.sh
