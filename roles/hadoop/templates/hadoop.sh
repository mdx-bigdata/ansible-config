export HADOOP_HOME={{hadoop_home}}
export PATH=$HADOOP_HOME/sbin:$HADOOP_HOME/bin:$PATH
export HADOOP_LOG_DIR={{hadoop_log_dir}}