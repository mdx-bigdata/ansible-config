---
- name: Create hadoop directories
  file:
    path: "{{item}}"
    state: directory
    owner: "{{hadoop_user}}"
    group: "{{hadoop_user}}"
    mode: 0755
  with_items: "{{hadoop_directories}}"
  tags:
    - hadoop

- name: Check if hadoop version {{hadoop_version}} is installed
  stat:
    path: "{{hadoop_home}}"
  register: hadoop_version_stat_result
  tags:
    - hadoop

- name: Download Hadoop version {{hadoop_version}}
  get_url:
    url: "{{hadoop_download_url}}"
    dest: "/tmp/hadoop-{{hadoop_version}}.tar.gz"
  when: hadoop_version_stat_result.stat.exists == false
  tags:
    - hadoop

- name: Unzip Hadoop version {{hadoop_version}}
  unarchive:
    src: "/tmp/hadoop-{{hadoop_version}}.tar.gz"
    dest: "{{hadoop_user_home}}"
    remote_src: yes
  when: hadoop_version_stat_result.stat.exists == false
  tags:
    - hadoop

- name: Fix permissions for hadoop binaries and config
  file:
    path: "{{hadoop_home}}"
    owner: "{{hadoop_user}}"
    group: "{{hadoop_user}}"
    recurse: yes
  tags:
    - hadoop

- name: Setup Hadoop variables
  template:
    src: hadoop.sh
    dest: /etc/profile.d
  tags:
    - hadoop

- name: Setup Hadoop core-site.xml config
  template:
    src: core-site.xml
    dest: "{{hadoop_config_path}}"
    owner: "{{hadoop_user}}"
    group: "{{hadoop_user}}"
    mode: 0644
  tags:
    - hadoop

- name: Setup Hadoop hdfs-site.xml config
  template:
    src: hdfs-site.xml
    dest: "{{hadoop_config_path}}"
    owner: "{{hadoop_user}}"
    group: "{{hadoop_user}}"
    mode: 0644
  tags:
    - hadoop

- name: Setup Hadoop mapred-site.xml config
  template:
    src: mapred-site.xml
    dest: "{{hadoop_config_path}}"
    owner: "{{hadoop_user}}"
    group: "{{hadoop_user}}"
    mode: 0644
  tags:
    - hadoop

- name: Setup Hadoop yarn-site.xml config
  template:
    src: yarn-site.xml
    dest: "{{hadoop_config_path}}"
    owner: "{{hadoop_user}}"
    group: "{{hadoop_user}}"
    mode: 0644
  tags:
    - hadoop

- name: Setup Hadoop workers
  template:
    src: workers
    dest: "{{hadoop_config_path}}"
    owner: "{{hadoop_user}}"
    group: "{{hadoop_user}}"
    mode: 0644
  tags:
    - hadoop

- name: Setup startup and stop hadoop scripts
  copy:
    src: "{{item}}"
    dest: "{{hadoop_binaries_path}}"
    owner: "{{hadoop_user}}"
    group: "{{hadoop_user}}"
    mode: 0764
  loop:
    - hadoop-start.sh
    - hadoop-stop.sh
  tags:
    - hadoop

- name: Setup '/bin/bash' shell for crontab commands
  cronvar:
    name: SHELL
    value: /bin/bash
    user: "{{hadoop_user}}"
  tags:
    - hadoop

- name: Creates hadoop crontab entry on primary
  cron:
    name: Start hadoop yarn and namenode after reboot
    special_time: reboot
    user: "{{hadoop_user}}"
    job: "{{hadoop_binaries_path}}/hadoop-start.sh"
  when: "'hadoop_master_primary' in inventory_hostname"
  tags:
    - hadoop

- name: Creates hadoop crontab entry on secondary
  cron:
    name: Start hadoop secondary namenode after reboot
    special_time: reboot
    user: "{{hadoop_user}}"
    job: "source {{hadoop_user_home}}/.bashrc && source /etc/profile.d/java.sh && source /etc/profile.d/hadoop.sh && hdfs --daemon start secondarynamenode"
  when: "'hadoop_master_secondary' in inventory_hostname"
  tags:
    - hadoop

- name: Removes secondary hadoop crontab entry on primary
  cron:
    name: Start hadoop secondary namenode after reboot
    special_time: reboot
    user: "{{hadoop_user}}"
    job: "source {{hadoop_user_home}}/.bashrc && source /etc/profile.d/java.sh && source /etc/profile.d/hadoop.sh && hdfs --daemon start secondarynamenode"
    state: absent
  when: "'hadoop_master_primary' in inventory_hostname"
  tags:
    - hadoop

- name: Removes primary hadoop crontab entry on secondary
  cron:
    name: Start hadoop yarn and namenode after reboot
    special_time: reboot
    user: "{{hadoop_user}}"
    job: "source {{hadoop_user_home}}/.bashrc && {{hadoop_binaries_path}}/hadoop-start.sh"
    state: absent
  when: "'hadoop_master_secondary' in inventory_hostname"
  tags:
    - hadoop

- name: Creates hadoop crontab entry on worker
  cron:
    name: Start hadoop datanode after reboot
    special_time: reboot
    user: "{{hadoop_user}}"
    job: "source {{hadoop_user_home}}/.bashrc && source /etc/profile.d/java.sh && source /etc/profile.d/hadoop.sh && hdfs --daemon start datanode"
  when: "'bigdataw' in inventory_hostname"
  tags:
    - hadoop
