---
- name: Create ldap directories
  file:
    path: "{{item}}"
    state: directory
    owner: "{{openldap_server_user}}"
    group: "{{openldap_server_user}}"
    mode: 0755
  with_items: "{{openldap_server_directories}}"
  tags:
    - ldap

- name: Add IP address of presto coordinator to /etc/hosts for ldap
  lineinfile:
    dest: /etc/hosts
    regexp: '.*{{openldap_server_domain_name}}$'
    line: "{{hostvars[inventory_hostname]['ansible_default_ipv4']['address']}} {{openldap_server_domain_name}}"
    state: present
  tags:
    - ldap

- name: Install openldap packages
  yum:
    name: "{{openldap_packages}}"
    state: present
  tags:
    - ldap

- name: Delete slapd.d configuration directory
  file:
    path: "{{openldap_server_config_path}}/slapd.d"
    state: absent
  tags:
    - ldap

- name: Create slapd.d configuration directory
  file:
    path: "{{openldap_server_config_path}}/slapd.d"
    state: directory
    owner: "{{openldap_server_user}}"
    group: "{{openldap_server_user}}"
    mode: 0755
  tags:
    - ldap

- name: Generate the root password for ldap
  shell: "slappasswd -s {{openldap_server_root_password}}"
  register: root_password
  no_log: true
  tags:
    - ldap

- name: Setup Openldap Server slapd.conf config
  template:
    src: slapd.conf
    dest: "{{openldap_server_config_path}}/slapd.conf"
  register: slapd_config
  when: root_password is changed
  tags:
    - ldap

- name: Setup Openldap Server ldap.conf config
  template:
    src: ldap.conf
    dest: "{{openldap_server_config_path}}/ldap.conf"
  tags:
    - ldap

- name: Setup the openssl template for creating ssl certificate
  template:
    src: openssl.cnf
    dest: "{{openldap_server_config_path}}"
  tags:
    - ldap

- name: Generate private CA key
  shell: |
    openssl genrsa \
      -aes256 \
      -passout pass:{{openldap_certificate_password}} \
      -out bigdatap.ca.key 4096
    chmod 400 bigdatap.ca.key
  args:
    chdir: "{{openldap_server_ca_certificates_path}}"
    creates: "{{openldap_server_ca_certificates_path}}/bigdatap.ca.key"
  tags:
    - ldap

- name: Generate CA key from private CA key
  shell: |
    echo -e "\n\n\n\n\n" | openssl req \
      -new \
      -x509 \
      -sha256 \
      -extensions v3_ca \
      -passin pass:{{openldap_certificate_password}} \
      -key bigdatap.ca.key \
      -out bigdatap.ca.pem \
      -days 7300 \
      -config {{openldap_server_config_path}}/openssl.cnf
  args:
    chdir: "{{openldap_server_ca_certificates_path}}"
    creates: "{{openldap_server_ca_certificates_path}}/bigdatap.ca.pem"
  tags:
    - ldap

- name: Generate private LDAP key
  shell: |
    openssl genrsa \
      -out ldap.key 4096
    chmod 400 ldap.key
  args:
    chdir: "{{openldap_server_certificates_path}}"
    creates: "{{openldap_server_certificates_path}}/ldap.key"
  tags:
    - ldap

- name: Create certificate signing request for LDAP key
  shell: |
    echo -e "\n\n\n\n\n\n" | openssl req \
      -new \
      -key ldap.key \
      -out ldap.csr \
      -config {{openldap_server_config_path}}/openssl.cnf
  args:
    chdir: "{{openldap_server_certificates_path}}"
    creates: "{{openldap_server_certificates_path}}/ldap.csr"
  tags:
    - ldap

- name: Sign certificate signing request with CA key
  shell: |
    openssl x509 \
      -req \
      -CA {{openldap_server_ca_certificates_path}}/bigdatap.ca.pem \
      -CAkey {{openldap_server_ca_certificates_path}}/bigdatap.ca.key \
      -CAcreateserial \
      -passin pass:{{openldap_certificate_password}} \
      -in ldap.csr \
      -out ldap.crt \
      -days 7300 \
      -sha256 \
      -extfile {{openldap_server_config_path}}/openssl.cnf
  args:
    chdir: "{{openldap_server_certificates_path}}"
    creates: "{{openldap_server_certificates_path}}/ldap.crt"
  tags:
    - ldap

- name: Add ca certificate in systems trusted store
  shell: |
    cp {{openldap_server_ca_certificates_path}}/bigdatap.ca.pem /etc/pki/ca-trust/source/anchors/ && \
    update-ca-trust
  args:
    creates: "/etc/pki/ca-trust/source/anchors/bigdatap.ca.pem"
  tags:
    - ldap

- name: Setup supporting slapd config
  copy:
    src: slapd
    dest: /etc/sysconfig/slapd
    mode: 0644
  when: openldap_server_enable_ssl
  register: supporting_lapd_config
  tags:
    - ldap

- name: Generate config files in slapd.d directory
  shell: |
    slaptest -f /etc/openldap/slapd.conf -F /etc/openldap/slapd.d/
  args:
    creates: "/etc/openldap/slapd.d/cn=config"
  tags:
    - ldap

- name: Enable manage access rights to external local connection 
  lineinfile:
    path: /etc/openldap/slapd.d/cn=config/olcDatabase={0}config.ldif
    regexp: '^olcAccess: \{0\}to'
    line: 'olcAccess: {0}to * by dn.exact=gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth manage by * break'
  tags:
    - ldap

- name: Fix checksum for config file
  shell: |
    DB_CONFIG_LDIF_CRC32=$(crc32 <(cat /etc/openldap/slapd.d/cn=config/olcDatabase={0}config.ldif | tail -n +3))
    DB_CONFIG_LDIF_CRC32="# CRC32 $DB_CONFIG_LDIF_CRC32"
    SED_RPL="'0,/# CRC32 .*/s//$DB_CONFIG_LDIF_CRC32/g'"
    eval "sed -i $SED_RPL /etc/openldap/slapd.d/cn=config/olcDatabase={0}config.ldif"
  tags:
    - ldap

- name: Setup DB_CONFIG file for LDAP database
  copy:
    src: /usr/share/openldap-servers/DB_CONFIG.example
    dest: "{{openldap_server_database_path}}/DB_CONFIG"
    owner: "{{openldap_server_user}}"
    group: "{{openldap_server_user}}"
    remote_src: yes

- name: Fix permissions for ldap configuration files
  file:
    path: "{{openldap_server_config_path}}"
    owner: "{{openldap_server_user}}"
    group: "{{openldap_server_user}}"
    recurse: yes
  tags:
    - ldap

- name: Restart slapd
  service:
    name: slapd
    state: restarted
  tags:
    - ldap

- name: Make sure slapd service is started and enabled
  service:
    name: slapd
    state: started
    enabled: yes
  tags:
    - ldap

- name: Setup the template for memberof configuration setup
  template:
    src: memberof_config.ldif
    dest: /tmp/
  register: memberof_config_template
  tags:
    - ldap

- name: Enable memberof configuration
  shell: |
    ldapadd -Y EXTERNAL -H ldapi:/// -D cn=admin,cn=config -f {{memberof_config_template.dest|default(memberof_config_template.path)}}
  tags:
    - ldap

- name: Setup the template for refint configuration setup
  template:
    src: refint_config.ldif
    dest: /tmp/
  register: refint_config_template
  tags:
    - ldap

- name: Enable refint configuration
  shell: |
    ldapadd -Y EXTERNAL -H ldapi:/// -D cn=admin,cn=config -f {{refint_config_template.dest|default(refint_config_template.path)}}
  tags:
    - ldap

- name: Restart slapd
  service:
    name: slapd
    state: restarted
  tags:
    - ldap

- name: Setup the template for creating base dn
  template:
    src: domain.ldif
    dest: /tmp/
  register: base_dn_template
  tags:
    - ldap

- name: Add the base domain
  shell: |
    ldapadd -H ldaps://{{openldap_server_domain_name}}/ -x -D "cn=Manager,{{openldap_server_base_dn}}" -w {{openldap_server_root_password}} -f {{base_dn_template.dest|default(base_dn_template.path)}} && touch {{openldap_server_config_path}}/rootdn_created
  args:
    creates: "{{openldap_server_config_path}}/rootdn_created"
  tags:
    - ldap

- include_tasks: organizational_unit.yml
  with_dict: "{{openldap_server_organizational_units}}"
  tags:
    - ldap

- include_tasks: ldap_user.yml
  with_dict: "{{ldap_users}}"
  tags:
    - ldap
    - ldap-user

- name: Setup the template for creating PrestoAccess user group
  template:
    src: userGroup.ldif
    dest: /tmp/
  register: user_group_template
  tags:
    - ldap
    - ldap-user

- name: Delete PrestoAccess user group if exists
  shell: |
    ldapdelete -c -H ldaps://{{openldap_server_domain_name}}/ -x -D "cn=Manager,{{openldap_server_base_dn}}" -w {{openldap_server_root_password}} {{openldap_server_group_dn}}
  ignore_errors: true
  tags:
    - ldap
    - ldap-user

- name: Create PrestoAccess user group
  shell: |
    ldapadd -H ldaps://{{openldap_server_domain_name}}/ -x -D "cn=Manager,{{openldap_server_base_dn}}" -w {{openldap_server_root_password}} -f {{user_group_template.dest|default(user_group_template.path)}}
  tags:
    - ldap
    - ldap-user
