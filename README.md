MDX Bigdata Devops
---

- [Preconditions](#preconditions)
- [Installation and Setup](#installation-and-setup)
    - [1. Hadoop Masters Setup](#1-hadoop-masters-setup)
    - [2. Presto Coordinator Setup](#2-presto-coordinator-setup)
    - [3. Hadoop Setup on Workers](#3-hadoop-setup-on-workers)
        - [3.1. Format the HDFS Namenode](#31-format-the-hdfs-namenode)
    - [4. Presto Setup on Workers](#4-presto-setup-on-workers)
- [Start or Stop Services](#start-or-stop-services)
    - [Start or Stop Hadoop](#start-or-stop-hadoop)
    - [Start or Stop Hive](#start-or-stop-hive)
    - [Start or Stop Presto](#start-or-stop-presto)
- [Create LDAP User](#create-ldap-user)


## Preconditions

1. Copy and setup `vars/postgresql.yml`:

        cp vars/postgresql.yml.example vars/postgresql.yml

    Edit the `vars/postgresql.yml` file and setup:

    - `hive_db_user`: PostgreSQL hive username (default: hive)
    - `hive_db_name`: PostgreSQL hive metastore database (default: metastore)
    - `hive_db_password`: PostgreSQL hive password

2. Copy and setup `vars/presto.yml`:

        cp vars/presto.yml.example vars/presto.yml

    Edit the `vars/presto.yml` file and setup:

    - `cassandra_username`: Cassandra connector username (default: deak)
    - `cassandra_password`: Cassandra connector password
    - `nbo_psql_username` : NBO PostgreSQL connector username (default: nbo)
    - `nbo_psql_password` : NBO PostgreSQL connector password
    - `java_key_store_password`: Java Key Store password
    - `openldap_server_root_password`: Openldap server root password
    - `openldap_certificate_password`: Openldap CA Certificate password to sign the main certificate

3. Setup hosts and hostgroups in 'hosts' file in the root of the project.

    Working example:

        [local_connection]
        localhost ansible_connection=local

        [hadoop_masters]
        hadoop_master_primary ansible_host=172.16.114.31
        hadoop_master_secondary ansible_host=172.16.114.32

        [presto_coordinator]
        172.16.114.33

        [bigdata_workers]
        bigdataw01.mediworx.local ansible_host=172.16.114.21
        bigdataw02.mediworx.local ansible_host=172.16.114.22
        bigdataw03.mediworx.local ansible_host=172.16.114.23

4. Setup hadoop masters preconditions - security, hosts, user, known hosts and ssh keys

        ansible-playbook hadoop-master_precond.yml -e REMOTE_USER=<your_user> --ask-pass

5. Setup presto coordinator preconditions - security, user

        ansible-playbook presto-coordinator_precond.yml -e REMOTE_USER=<your_user> --ask-pass

6. Setup workers preconditions - security, hosts, users, known hosts, storage mounts and ssh keys

        ansible-playbook bigdata-worker_precond.yml -e REMOTE_USER=<your_user> --ask-pass


## Installation and Setup

### 1. Hadoop Masters Setup

Install and setup hadoop on hadoop masters - java, postgresql, hive, hadoop (core, hdfs, mapred, yarn, workers)

    ansible-playbook hadoop-master.yml -e REMOTE_USER=<your_user> --ask-pass

_Hint_: You can use tags to run only particular installations:

- java and postgresql installation and setup:

        ansible-playbook hadoop-master.yml --tags "java,postgresql" -e REMOTE_USER=<your_user> --ask-pass

- hadoop installation and setup:

        ansible-playbook hadoop-master.yml --tags "hadoop" -e REMOTE_USER=<your_user> --ask-pass

- java and hadoop installation and setup:

        ansible-playbook hadoop-master.yml --tags "java,hadoop" -e REMOTE_USER=<your_user> --ask-pass

- postgresql and hive installation and setup:

        ansible-playbook hadoop-master.yml --tags "postgresql,hive" -e REMOTE_USER=<your_user> --ask-pass


### 2. Presto Coordinator Setup

1. Add presto coordinator tar file with the name like `presto-server-0.220.tar.gz` in `roles/presto/files` and set correct `presto_version` in `vars/common.yml` file
2. Install and setup presto coordinator - java, ldap, ldap users and presto

        ansible-playbook presto-coordinator.yml -e REMOTE_USER=<your_user> --ask-pass

    _Hint_: You can use tags to run only particular installations:

    - java installation and setup:
        
            ansible-playbook presto-coordinator.yml --tags "java" -e REMOTE_USER=<your_user> --ask-pass

    - ldap installation and setup with ldap users:
        
            ansible-playbook presto-coordinator.yml --tags "ldap" -e REMOTE_USER=<your_user> --ask-pass

    - only add ldap users:
        
            ansible-playbook presto-coordinator.yml --tags "ldap-user" -e REMOTE_USER=<your_user> --ask-pass
    
        _Hint_: Create password for all newly created users (login to presto coordinator server):

            ldappasswd -H ldaps://ldap.bigdata.mediworx.local/ -x -D 'cn=Manager,dc=ldap,dc=bigdata,dc=mediworx,dc=local' -W -S 'uid=<uid.name>,ou=Users,dc=ldap,dc=bigdata,dc=mediworx,dc=local'

    - presto installation and setup with java keystore and TLS:
        
            ansible-playbook presto-coordinator.yml --tags "presto" -e REMOTE_USER=<your_user> --ask-pass

### 3. Hadoop Setup on Workers

Install and setup hadoop on workers - java, hadoop (core, hdfs, mapred, yarn, workers)

    ansible-playbook bigdata-worker.yml --tags "java,hadoop" -e REMOTE_USER=<your_user> --ask-become-pass --ask-pass

_Hint_: You can use tags to run only particular installations:

- java installation:

        ansible-playbook bigdata-worker.yml --tags "java" -e REMOTE_USER=<your_user> --ask-become-pass --ask-pass

- hadoop installation:

        ansible-playbook bigdata-worker.yml --tags "hadoop" -e REMOTE_USER=<your_user> --ask-become-pass --ask-pass

#### 3.1. Format the HDFS Namenode

<span style="color:red">**IMPORTANT AND DANGEROUS**</span> - Format the HDFS namenode ONCE and only during the first run. The command will hang after you run it on the formatted namenode!

    ansible-playbook hadoop-actions.yml -e SWITCH=format -e REMOTE_USER=<your_user> --ask-become-pass --ask-pass

### 4. Presto Setup on Workers

Install and setup presto on workers - java, presto

    ansible-playbook bigdata-worker.yml --tags "java,presto" -e REMOTE_USER=<your_user> --ask-become-pass --ask-pass

_Hint_: You can use tags to run only particular installations:

- java installation:

        ansible-playbook bigdata-worker.yml --tags "java" -e REMOTE_USER=<your_user> --ask-become-pass --ask-pass

- presto installation:

        ansible-playbook bigdata-worker.yml --tags "presto" -e REMOTE_USER=<your_user> --ask-become-pass --ask-pass


## Start or Stop Services

### Start or Stop Hadoop

Start (or Stop) Hadoop using `SWITCH=start` (or `SWITCH=stop`)

    ansible-playbook hadoop-actions.yml -e SWITCH=start -e REMOTE_USER=<your_user> --ask-become-pass --ask-pass

### Start or Stop Hive

Start (or Stop) Hive using `SWITCH=start` (or `SWITCH=stop`)

    ansible-playbook hive-actions.yml -e SWITCH=start -e REMOTE_USER=<your_user> --ask-become-pass --ask-pass

### Start or Stop Presto

Start (or Stop) Presto using `SWITCH=start` (or `SWITCH=stop`)

    ansible-playbook presto-actions.yml -e SWITCH=start -e REMOTE_USER=<your_user> --ask-become-pass --ask-pass


## Create LDAP User

1. Define the new user in `ldap_users` dictionary (`vars/ldap-users.yml` file):

    For example `sample.user` with uid `1110` and gid `1110`:

        ldap_users:
          sample.user:
            ldap_cn: Sample User
            ldap_uid: sample.user
            ldap_uid_number: 1110
            ldap_gid_number: 1110
            ldap_home_directory: /home/sample.user
            ldap_gecos: sample.user

    _Important_: UID and GID have to be unique and starting from 1100.

2. Add user in the correct access group in `presto_access_rules` dictionary in the file `vars/presto-access-rules.yml`:

    Add user to the correct group:

    For example to add read-write permissions to `cassandra.analytics` schema for user `sample.user`:

        cassandra_analytics_rw:
        catalog_schema: cassandra.analytics
        permissions: rw
        ldap_users:
            - sample.user

    Or to add read-only permissions to `hive.default` schema for user `other.user` and `sample.user`:

        hive_default_r:
        catalog_schema: hive.default
        permissions: r
        ldap_users:
            - other.user
            - sample.user

3. Create defined users with access rules using ansible playbook - idempotent run - if the user is already created, ansible will skip creation

        ansible-playbook presto-coordinator.yml --tags "ldap-user" -e REMOTE_USER=<your_user> --ask-pass
        ansible-playbook bigdata-worker.yml --tags "ldap-user" -e REMOTE_USER=<your_user> --ask-become-pass --ask-pass

4. Create password for all newly created users (login to presto coordinator server):

    For example new password for `sample.user`:

    a) Login as root to presto coordinator server
    
    b) Create new ldap password using command:

        ldappasswd -H ldaps://ldap.bigdata.mediworx.local/ -x -D 'cn=Manager,dc=ldap,dc=bigdata,dc=mediworx,dc=local' -W -S 'uid=sample.user,ou=Users,dc=ldap,dc=bigdata,dc=mediworx,dc=local'

    c) The command will ask for a new password for the `sample.user` and root ldap password (stored as `openldap_server_root_password` in `vars/presto.yml` file)

    b) Run presto configuration on presto coordinator with tags `ldap-user`
